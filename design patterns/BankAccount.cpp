#include "BankAccount.h"

BankAccount::BankAccount(const double & balance) : m_balance(balance)
{
}

BankAccount::~BankAccount()
{
}

void BankAccount::Withdraw(const double & amount)
{
	m_balance -= amount;
}

void BankAccount::Deposit(const double & amount)
{
	m_balance += amount;
}

double BankAccount::GetBalance() const
{
	return m_balance;
}
