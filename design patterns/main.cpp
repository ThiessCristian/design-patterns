#include "BasicPrinter.h"
#include "TextFormater.h"
#include "CaseFormater.h"
#include "BorderFormater.h"

int main()
{
   std::vector<std::string> lines{"hello world","yes!"};
   
   ITextPrinter* f = new BorderFormater(
   		new BorderFormater(new CaseFormater(
			new BasicPrinter())), '#');

	f->Print(lines);
	return EXIT_SUCCESS;
}