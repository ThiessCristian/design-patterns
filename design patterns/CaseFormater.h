#pragma once
#include "TextFormater.h"
class CaseFormater :
   public TextFormater
{
public:

   void Print(std::vector<std::string> lines) override;

   CaseFormater(ITextPrinter* textPrinter);
   ~CaseFormater();
};

