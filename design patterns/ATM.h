#pragma once
#include "BankAccount.h"
class ATM :
	public IBankAccount
{
	constexpr static const double WITHDRAW_LIMIT = 1000;
	BankAccount* m_account;
public:
	ATM();
	~ATM();

	// Inherited via IBankAccount
	virtual void Withdraw(const double & amount) override;
	virtual void Deposit(const double & amount) override;
	virtual double GetBalance() const override;
};

