#include "BorderFormater.h"
#include <algorithm>


void BorderFormater::Print(std::vector<std::string> lines)
{
   const auto maxchars = std::max_element(
      lines.begin(),
      lines.end(),
      [](const auto& a, const auto& b) { return a.length() < b.length(); }
   )->length();  

   std::string border(maxchars, m_borderChar);

   lines.insert(lines.begin(), border);
   lines.insert(lines.end(), border);

   TextFormater::Print(lines);
}

BorderFormater::BorderFormater(ITextPrinter* textPrinter, const char& borderChar):TextFormater(textPrinter), m_borderChar(borderChar)
{}


BorderFormater::~BorderFormater()
{}
