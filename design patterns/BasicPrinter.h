#pragma once

#include "ITextPrinter.h"

class BasicPrinter :
   public ITextPrinter
{
public:
   BasicPrinter();
   ~BasicPrinter();

   void Print(std::vector<std::string> lines) override;
};

