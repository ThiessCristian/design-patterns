#pragma once
class IBankAccount
{
public:
	virtual void Withdraw(const double& amount) = 0;
	virtual void Deposit(const double& amount) = 0;
	virtual double GetBalance() const = 0;
};

