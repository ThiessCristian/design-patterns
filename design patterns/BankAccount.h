#pragma once
#include "IBankAccount.h"
class BankAccount :
	public IBankAccount
{
	double m_balance;
public:
	BankAccount(const double& balance = 0);
	~BankAccount();

	// Inherited via IBankAccount
	virtual void Withdraw(const double & amount) override;
	virtual void Deposit(const double & amount) override;
	virtual double GetBalance() const override;
};

