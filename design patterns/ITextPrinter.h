#pragma once
#include <vector>
#include <string>


class ITextPrinter
{
public:
   virtual void Print(std::vector<std::string> lines) = 0;

};
