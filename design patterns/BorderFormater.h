#pragma once
#include "TextFormater.h"
class BorderFormater :
   public TextFormater
{
	char m_borderChar;
public:

   void Print(std::vector<std::string> lines) override;
   BorderFormater(ITextPrinter* textPrinter, const char& borderChar='*');
   ~BorderFormater();
};

