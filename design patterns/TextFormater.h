#pragma once

#include "ITextPrinter.h"

class TextFormater :
   public ITextPrinter
{
   ITextPrinter* m_textPrinter;

public:

   void Print(std::vector<std::string> lines) override;


   TextFormater(ITextPrinter* textPrinter);
   ~TextFormater();
};

