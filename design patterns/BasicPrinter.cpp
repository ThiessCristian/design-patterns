#include "BasicPrinter.h"
#include <iostream>


BasicPrinter::BasicPrinter()
{}


BasicPrinter::~BasicPrinter()
{}

void BasicPrinter::Print(std::vector<std::string> lines)
{
   for (const auto& line : lines) {
      std::cout << line << std::endl;
  }
}
