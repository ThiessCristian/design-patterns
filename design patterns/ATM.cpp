#include "ATM.h"
#include <exception>

//const double ATM::WITHDRAW_LIMIT = 1000;

ATM::ATM():m_account(new BankAccount())
{
}


ATM::~ATM()
{
}

void ATM::Withdraw(const double & amount)
{
	if (amount > WITHDRAW_LIMIT)
	{
		throw std::exception("Amount exceeds withdraw limit");
	}
	if (amount > m_account->GetBalance())
	{
		throw std::exception("Amount exceeds your balance");
	}
	if (amount < 0)
	{
		throw std::exception("Amount is not valid");
	}
	m_account->Withdraw(amount);
}

void ATM::Deposit(const double & amount)
{
	if (amount < 0)
	{
		throw std::exception("Amount is not valid");
	}
	m_account->Deposit(amount);
}

double ATM::GetBalance() const
{
	return m_account->GetBalance();
}
