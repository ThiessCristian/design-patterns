#include "ATM.h"
#include <iostream>

void main()
{
	IBankAccount * atm = new ATM();
	atm->Deposit(500);

	try 
	{
		atm->Withdraw(5000000);
	} catch(std::exception ex)
	{
		std::cout << ex.what()<<std::endl;
	}

	try
	{
		atm->Withdraw(600);
	}
	catch (std::exception ex)
	{
		std::cout << ex.what() << std::endl;
	}

	try
	{
		atm->Withdraw(-1);
	}
	catch (std::exception ex)
	{
		std::cout << ex.what() << std::endl;
	}

	try
	{
		atm->Withdraw(300);
		std::cout << "Final balance: " << atm->GetBalance() << std::endl;
	}
	catch (std::exception ex)
	{
		std::cout << ex.what() << std::endl;
	}
}
