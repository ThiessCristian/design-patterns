#include "CaseFormater.h"
#include <algorithm>

void CaseFormater::Print(std::vector<std::string> lines)
{
   for (auto& line : lines) {
      std::transform(line.begin(), line.end(), line.begin(), ::toupper);
   }

   TextFormater::Print(lines);
}


CaseFormater::CaseFormater(ITextPrinter * textPrinter):TextFormater(textPrinter)
{}

CaseFormater::~CaseFormater()
{}
